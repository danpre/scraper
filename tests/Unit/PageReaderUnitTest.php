<?php

namespace Tests\Unit;

use App\Acme\Scraper\Readers\PageReader;
use Symfony\Component\DomCrawler\Crawler;
use Tests\Helpers\PageUtils;
use Tests\TestCase;

class PageReaderUnitTest extends TestCase
{
  use PageUtils;

   /** @test */
  function a_reader_can_get_a_list_of_titles(){
    $reader = new PageReader();
    //create a page with 3 title links
    $crawler = new Crawler($this->pageMock());
    $this->assertEquals(3, $reader->titleLinks($crawler)->count());
  }

  /** @test */
  function a_link_is_considered_release_category_link(){
    $crawler = new Crawler($this->releaseLinks(1));
    $reader = new PageReader();
    $this->assertTrue( $reader->isReleaseLink($reader->titleLinks($crawler)->first()));
  }


  /** @test */
  function a_reader_can_get_a_list_of_releases_category_urls(){
    $reader = new PageReader();
    //Given a page with 5 links out of which 4 are Release links
    $crawler = $this->crawler(5,4);
    $items = $reader->releaseLinks($crawler);
    $this->assertCorrectUrls($items->map->url->toArray(), 4);
  }

  /** @test */
  function a_link_is_discarded_if_not_release_category_link(){
    $crawler = new Crawler($this->otherLinks(1));
    $reader = new PageReader();
    $this->assertFalse( $reader->isReleaseLink($reader->titleLinks($crawler)->first()));
  }

  /** @test */
  function a_reader_can_get_all_category_link_from_a_page(){
    $page = new PageReader();
    $items = $page->read($this->crawler(20,10));
    $this->assertCorrectUrls($items->map->url->toArray(), 10);
  }

  /**
   * @param $urls
   * @param int $howMany
   */
  public function assertCorrectUrls(array $urls, $howMany){
    $this->assertCount($howMany, $urls);
    array_map(function ($url) {
      $this->assertNotEmpty($url);
      $this->assertTrue(!!filter_var($url, FILTER_VALIDATE_URL));
    }, $urls);
  }
}
