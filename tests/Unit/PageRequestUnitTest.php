<?php

namespace Tests\Unit;

use App\Acme\Scraper\Exceptions\InvalidUrlException;
use App\Acme\Scraper\Requests\PageRequest;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PageRequestUnitTest extends TestCase
{
    /** @test */
    function a_page_request_must_throw_error_on_invalid_url(){
      $url = "this is not a valid url";
      $this->expectException(InvalidUrlException::class);
      new PageRequest($url);
    }
}
