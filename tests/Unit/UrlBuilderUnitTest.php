<?php

namespace Tests\Unit;

use App\Acme\Scraper\Exceptions\InvalidUrlException;
use App\Acme\Scraper\Helpers\UrlBuilder;
use Tests\TestCase;

class UrlBuilderUnitTest extends TestCase
{

  const URL = "http://builder.example";

  /** @test */
    function a_builder_throws_an_exception_on_an_invalid_url(){
      $url = "this is not a valid url";
      $this->expectException(InvalidUrlException::class);
      $this->builder($url);
    }

    /** @test */
    function a_builder_may_append_to_an_url(){
      $value = $this->builder()
        ->append("one", "two", 'three')
        ->raw();
      $this->assertEquals(self::URL .'/one/two/three', $value);
    }


  /** @test */
  function a_builder_no_empty_slashes(){
    $value = $this->builder(self::URL . "////")
      ->raw();
    $this->assertEquals(self::URL, $value);
  }

  /**
   * @param string $url
   *
   * @return \App\Acme\Scraper\Helpers\UrlBuilder
   * @throws \App\Acme\Scraper\Exceptions\InvalidUrlException
   */
    function builder($url = self::URL){
      return new UrlBuilder($url);
    }
}
