<?php

namespace Tests\Unit;

use App\Acme\Scraper\Readers\PostReader;
use DonatelloZa\RakePlus\RakePlus;
use Symfony\Component\DomCrawler\Crawler;
use Tests\Helpers\PageUtils;
use Tests\TestCase;

class PostReaderUnitTest extends TestCase {

  use PageUtils;

  /** @test */
  function a_reader_may_retrieve_head_tag() {
    $crawler = $this->crawler();
    $reader = new PostReader;
    $this->assertCount(4, $reader->head($crawler)->children());
  }

  /** @test */
  function a_reader_may_retrieve_head_title_tag() {
    $crawler = $this->crawler();
    $reader = new PostReader;
    $this->assertEquals("Some title", $reader->headTag($crawler, 'title')
      ->text());
  }

  /** @test */
  function a_reader_may_retrieve_head_meta_title_tag() {
    $crawler = $this->crawler();
    $reader = new PostReader;
    $this->assertEquals("Some title", $reader->metaTitle($crawler));
  }

  /** @test */
  function a_reader_may_retrieve_head_meta_description_tag() {
    $crawler = $this->crawler();
    $reader = new PostReader;
    $this->assertEquals("Very long and tiresome description about how cool this release is, but we all now Laravel is better", $reader->metaDescription($crawler));
  }

  /** @test */
  function a_reader_may_retrieve_the_body() {
    $crawler = $this->postCrawler();
    $reader = new PostReader;
    $this->assertEquals("An even longer text  here filled with links, language or even images.", $reader->body($crawler));
  }

  function a_reader_can_retrieve_keywords_from_meta(){
    $dom = <<<HTML
<html><head><meta property="og:keywords" content="key1, key2, key3" /></head></html>
HTML;
    $reader = new PostReader;
    $keywords = $reader->metaKeywords(new Crawler($dom));
    $this->assertEqualArrays(['key1', 'key2', 'key3'], $keywords);
  }

  /** @test */
  function a_reader_can_retrieve_keywords_if_absent_from_meta(){
    $crawler = $this->postCrawler();
    $reader = new PostReader;
    $expectedKeywords = RakePlus::create("An even longer text  here filled with links, language or even images.")
      ->sortByScore(RakePlus::ORDER_DESC)
      ->get();
    $keywords = $reader->metaKeywords($crawler);
    $this->assertEqualArrays($expectedKeywords, $keywords);
  }

  /**
   * Assert equals array
   *
   * @param array $expected
   * @param array $actual
   *
   * @return $this
   */
  function assertEqualArrays(array $expected, array $actual){
    $this->assertCount(count($expected), $actual);
    foreach ($expected as $key => $value ){
      $this->assertEquals($value, $actual[$key]);
    }
    return $this;
  }

  /**
   * Init crawler with a post page
   *
   * @return \Symfony\Component\DomCrawler\Crawler
   */
  function postCrawler(){
    return new Crawler($this->postPageMock());
  }
}
