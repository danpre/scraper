<?php

namespace Tests\Feature;

use Tests\TestCase;

class ScraperRequestsTest extends TestCase
{

    /** @test */
  public function a_list_of_results_must_be_presented()
  {
    $this->json("get", "/api/scrape")->assertSuccessful()->assertJsonStructure([
      'results' => [
          '*' => [
            'url',
            'link',
            'meta description',
            'keywords',
            'filesize'
          ]
      ],
      'total',
    ]) ;
  }
}
