<?php
/**
 * Created by PhpStorm.
 * User: erlik28
 * Date: 5/26/18
 * Time: 9:13 AM
 */

namespace Tests\Helpers;


use App\Acme\Scraper\Readers\PageReader;
use function collect;
use Illuminate\Foundation\Testing\WithFaker;
use Symfony\Component\DomCrawler\Crawler;

trait PageUtils {

  use WithFaker;

  /**
   * Mock a page and return it's temp url
   *
   * @param int $numberOfItems
   * @param int $correctItems
   *
   * @return string
   */
  function pageMock($numberOfItems = 3, $correctItems = 1) {
    $body = $this->otherLinks($numberOfItems - $correctItems);
    $body .= $this->releaseLinks($correctItems);
    return $this->mockBodyWrapping($body);
  }

  function postPageMock(){
    $body = <<<HTML
    <div class="storycontent">
      <p>An even longer text  here filled with <a href="">links</a>, <strong>language</strong> or even images<div class="awsome-position"><img src="http://some.image/url" /></div>.
      </p>
   
    </div>
HTML;
    return $this->mockBodyWrapping($body);
  }

  /**
   * Instantiate a crawler already loaded with a mock page
   *
   * @param $numberOfItems
   * @param $correctItems
   *
   * @return \Symfony\Component\DomCrawler\Crawler
   */
  function crawler($numberOfItems = 3, $correctItems = 1) {
    return new Crawler($this->pageMock($numberOfItems, $correctItems));
  }



  /**
   * @return string
   */
  function goodUrl() {
    return "https://wordpress.org/news/category/releases/";
  }


  /**
   * Mock the wrapping tags
   *
   * @param $content
   *
   * @return string
   */
  function mockBodyWrapping($content) {
    return <<<HTML
<!DOCTYPE html>
<html>
<head>
<meta property="og:title" content="Some title" />
<title>Some title</title>
<meta property="og:url" content="https://wordpress.org/news/2018/05/wordpress-4-9-6-privacy-and-maintenance-release/" />
<meta property="og:description" content="Very long and tiresome description about how cool this release is, but we all now Laravel is better" />
</head>
<body>
<div id="pagebody">
    <div class="wrapper">
    <div class="col-9">
    $content
    </div>
    </div>
    </div>
</body>
</html>
HTML;
  }

  /**
   * Release link group
   *
   * @param int $howMany
   *
   * @return string
   */
  function releaseLinks($howMany = 1) {
    return $this->wpPosts($howMany,
      "Wp-214 Released",
      PageReader::RELEASE_TEXT,
      $this->goodUrl())
      ->implode("");
  }

  /**
   * Normal link group that isn't a release
   *
   * @param int $howMany
   *
   * @return string
   */
  function otherLinks($howMany = 1) {
    return $this->wpPosts($howMany, "Some other links", "Other category")
      ->implode("");
  }

  /**
   * Build a collection of posts
   *
   * @param int $howMany
   * @param null $title
   * @param null $categoryLinkTitle
   * @param null $categoryLinkUrl
   * @param null $url
   *
   * @return \Illuminate\Support\Collection
   */
  function wpPosts($howMany = 1, $title = NULL, $categoryLinkTitle = NULL, $categoryLinkUrl = NULL, $url = NULL) {
    $posts = collect();
    for ($i = 0; $i < $howMany; $i++) {
      $posts->push($this->wpPost($title, $url, $categoryLinkTitle, $categoryLinkUrl));
    }
    return collect($posts);
  }

  /**
   * Build a mock post
   *
   * @param null $title
   * @param null $url
   *
   * @param string $categoryLinkTitle
   * @param string $categoryLinkUrl
   *
   * @return string
   */
  function wpPost($title = NULL, $url = NULL, $categoryLinkTitle = NULL, $categoryLinkUrl = NULL) {
    if (!$title) {
      $title = $this->faker->word;
    }
    if (!$url) {
      $url = $this->faker->url;
    }
    if (!$categoryLinkUrl) {
      $categoryLinkUrl = $this->faker->url;
    }
    if (!$categoryLinkTitle) {
      $categoryLinkTitle = $this->faker->word;
    }
    return <<<HTML
<h2 class="fancy">
<a href="$url">$title</a>
</h2>

<div class="meta">Posted May 25, 2018 by 
<a href="#">Poster</a>. Filed under <a href="$categoryLinkUrl" rel="category tag">$categoryLinkTitle</a>. 
</div>

<div class="storycontent">
<p>Some text</p>
<a href="#">Poster</a>. Filed under <a href="$categoryLinkUrl" rel="category tag">Category</a>. 
<a href="#">Poster</a>. Filed under <a href="$categoryLinkUrl" rel="category tag">Category</a>. 
<ul>
<li>
<a href="#">Poster</a>. Filed under <a href="$categoryLinkUrl" rel="category tag">Category</a>. 
</li>
</ul>
</div>

HTML;

  }
}