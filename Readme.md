# Wordpress Scraper

Uses the power of Laravel to scrape the latest Wordpress releases.

The main dependency is fabpot/goutte, used to parse doms and extract the data.

Installation:

    git clone https://danpre@bitbucket.org/danpre/scraper.git
    cp .env.example .env
    php artisan key:generate
    php artisan serve

By accessing `http://localhost:8000` you should be able to see the website


#Usage

On the home page there is a little Vue component that does the fetching and provides minimal caching and navigation (the design is, obviously, very-programmery-looking).

Also uses Freek Van der Herten's Response caching package (so I waste less of your time), so after an initial warm-up should work quite fast.
It's set to hold the data for 24h.


To clear the backend cache please use `php artisan responsecache:clear`.


For the raw json files please visit `http://localhost:8000/api/scrape`, with an optional `page` param: 
`http://localhost:8000/api/scrape?page=1`


 #Notes
 - As the release pages did not provide actual `Content-Lenght` headers, I had to use a different client for retrieving the Dom (`GuzzleHttp\Client`)
 - As there were no keywords in the head or body of the realease pages, I defaulted the keyword extraction to a Rake class (however, if the keywords ever appear, they will be read)