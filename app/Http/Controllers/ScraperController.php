<?php

namespace App\Http\Controllers;

use App\Acme\Scraper\Exceptions\InvalidUrlException;
use App\Acme\Scraper\Scraper;
use App\Acme\Scraper\ScraperPresenter;
use function get_class;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Http\Request;
use function response;

class ScraperController extends Controller
{

  /**
   * Runs the scraper
   *
   * @param \Illuminate\Http\Request $request
   *
   * @return \App\Acme\Scraper\ScraperPresenter|\Illuminate\Http\JsonResponse
   */
  function scrape(Request $request){
      $this->validate($request, ['page' => "numeric"]);
      try{
        return new ScraperPresenter(Scraper::run($request->page));
      }catch (InvalidUrlException $exception){
          return $this->respondError("Sorry about that, we might have messed up the URL");
      }catch (ClientException $e){
        $message = $e->getCode() == 404 ? "I think that's a little too far." : "This one is on them.";
        return $this->respondError($message);
      }
    }

  /**
   * Display a minimal UI
   *
   * @return \Illuminate\Http\Response
   */
    function show(){
      return view("scraper.result");
    }

    function respondError($message = "Well..., that didn't go well."){
      return response()->json(['error' => $message]);
    }

}
