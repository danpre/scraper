<?php
/**
 * Created by PhpStorm.
 * User: erlik28
 * Date: 5/26/18
 * Time: 6:50 AM
 */

namespace App\Acme\Scraper;

use App\Acme\Scraper\Contracts\ReadearableContract;
use App\Acme\Scraper\Contracts\RequestableContract;
use GuzzleHttp\Client;
use Symfony\Component\DomCrawler\Crawler;

class RepositoryFactory {

  /**
   * @var \App\Acme\Scraper\Contracts\ReadearableContract
   */
  private $readearableContract;

  /**
   * @var \GuzzleHttp\Client
   */
  private $client;

  /**
   * @var \App\Acme\Scraper\Contracts\RequestableContract
   */
  private $request;


  /**
   * RepositoryFactory constructor.
   *
   * @param \App\Acme\Scraper\Contracts\RequestableContract $request
   * @param \App\Acme\Scraper\Contracts\ReadearableContract $reader
   */
  private function __construct(RequestableContract $request, ReadearableContract $reader) {
    $this->readearableContract = $reader;
    $this->request = $request;
  }

  /**
   * Named runner
   *
   * @param \App\Acme\Scraper\Contracts\RequestableContract $request
   * @param \App\Acme\Scraper\Contracts\ReadearableContract $readearableContract
   *
   * @return mixed
   */
  public static function make(RequestableContract $request, ReadearableContract $readearableContract) {
    $scraper = new static($request, $readearableContract);
    return $scraper->build();
  }

  /**
   * Runs the scraper
   *
   * @return array
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  function build(){
    $response = $this->makeRequest();
    $crawler = $this->makeCrawler($response->getBody()->getContents());
    return $this->readearableContract->read($crawler, $response);
  }

  /**
   * @return \GuzzleHttp\Client
   */
  function client(){
    if (!$this->client){
      $this->client = new Client();
    }
    return $this->client;
  }

  /**
   * @param string $body
   *
   * @return \Symfony\Component\DomCrawler\Crawler
   */
  public function makeCrawler($body) {
    $crawler = new Crawler($body);
    return $crawler;
  }

  /**
   * @return mixed|\Psr\Http\Message\ResponseInterface
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function makeRequest() {
    $response = $this->client()
      ->request($this->request->method(), $this->request->url());
    return $response;
  }


}