<?php
/**
 * Created by PhpStorm.
 * User: erlik28
 * Date: 5/26/18
 * Time: 6:55 AM
 */

namespace App\Acme\Scraper\Contracts;

/**
 * Interface RequestableContract
 * Forces minor validations to a url string
 *
 * @package App\Acme\RepositoryFactory\Contracts
 */
interface RequestableContract {

  /**
   * Return the raw url string
   *
   * @return string
   */
  function url();

  /**
   * Request method
   *
   * @return string
   */
  function method();
}