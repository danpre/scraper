<?php
/**
 * Created by PhpStorm.
 * User: erlik28
 * Date: 5/28/18
 * Time: 1:06 PM
 */

namespace App\Acme\Scraper\Contracts;

/**
 * Interface PresentableContract
 *
 * @package App\Acme\Scraper\Contracts
 */
interface PresentableContract {

  /**
   * @return array
   */
  function toArray();

  /**
   * @return string
   */
  function formattedFileSize();
}