<?php
/**
 * Created by PhpStorm.
 * User: erlik28
 * Date: 5/26/18
 * Time: 6:52 AM
 */

namespace App\Acme\Scraper\Contracts;

use Psr\Http\Message\ResponseInterface;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Interface ReadearableContract
 *
 * Should be able to read the desired information from a Crawler instance
 *
 * @package App\Acme\RepositoryFactory\Contracts
 */
interface ReadearableContract {

  function read(Crawler $crawler, ResponseInterface $response = null);
}