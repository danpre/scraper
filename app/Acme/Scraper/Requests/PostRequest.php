<?php
/**
 * Created by PhpStorm.
 * User: erlik28
 * Date: 5/27/18
 * Time: 11:10 AM
 */

namespace App\Acme\Scraper\Requests;

use App\Acme\Scraper\Contracts\RequestableContract;
use App\Acme\Scraper\Helpers\UrlBuilder;

class PostRequest implements RequestableContract {

  /**
   * @var \App\Acme\Scraper\Helpers\UrlBuilder
   */
  private $url;

  /**
   * PostRequest constructor.
   *
   * @param string $url
   *
   * @throws \App\Acme\Scraper\Exceptions\InvalidUrlException
   */
  public function __construct($url) {
    $this->url = new UrlBuilder($url);
  }


  /**
   * Return the raw url string
   *
   * @return string
   */
  function url() {
    return $this->url->raw();
  }

  /**
   * Request method
   *
   * @return string
   */
  function method() {
    return 'get';
  }
}