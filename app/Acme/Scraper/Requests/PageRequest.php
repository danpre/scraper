<?php
/**
 * Created by PhpStorm.
 * User: erlik28
 * Date: 5/26/18
 * Time: 7:34 AM
 */

namespace App\Acme\Scraper\Requests;


use App\Acme\Scraper\Contracts\RequestableContract;
use App\Acme\Scraper\Helpers\UrlBuilder;

class PageRequest implements RequestableContract {

  /**
   * @var \App\Acme\Scraper\Helpers\UrlBuilder
   */
  private $url;

  /**
   * @var int
   */
  private $pageNumber;

  /**
   * PageRequest constructor.
   *
   * @param $url
   * @param int $pageNumber
   *
   * @throws \App\Acme\Scraper\Exceptions\InvalidUrlException
   */
  public function __construct($url = null , $pageNumber = 0) {
    $this->pageNumber = $pageNumber;
    if (!$url){
      $url = self::baseUrl();
    }
    $this->url = new UrlBuilder($url);
  }

  /**
   * Page number fluent setter
   *
   * @param int $pageNumber
   *
   * @return static
   */
  public function setPage($pageNumber){
    $this->pageNumber = $pageNumber;
    return $this;
  }

  /**
   * Return the raw url string
   *
   * @return string
   */
  function url() {
    return $this->pageNumber >= 2 ? $this->url->append("page", $this->pageNumber)->raw() : $this->url->raw();
  }

  /**
   * Request method
   *
   * @return string
   */
  function method() {
    return "GET";
  }

  public static function baseUrl() {
    return env('SCRAPER_BASE_URL', "https://wordpress.org/news/");
  }

}