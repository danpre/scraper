<?php
/**
 * Created by PhpStorm.
 * User: erlik28
 * Date: 5/26/18
 * Time: 7:49 AM
 */

namespace App\Acme\Scraper\Readers;


use App\Acme\Scraper\Contracts\ReadearableContract;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\DomCrawler\Crawler;
use function array_map;
use function collect;

/**
 * Class PageReader
 * Extracts the list "Release" nodes from a page
 *
 * @package App\Acme\RepositoryFactory
 */
class PageReader implements ReadearableContract {

  /**
   * The text a link must contain for be filed under the "Releases" category
   *
   * @var string
   */
  const RELEASE_TEXT = "Releases";

  /**
   * Get a collection of links from the Crawler
   *
   * @param \Symfony\Component\DomCrawler\Crawler $crawler
   *
   * @param \Psr\Http\Message\ResponseInterface $response
   *
   * @return \Illuminate\Support\Collection
   */
  function read(Crawler $crawler, ResponseInterface $response = null) {
    return $this->releaseLinks($crawler);
  }

  /**
   * @param \Symfony\Component\DomCrawler\Crawler $node
   * @param $selector
   *
   * @return \Symfony\Component\DomCrawler\Crawler
   */
  function nextParentsChild(Crawler $node, $selector){
    return $node
      ->parents()
      ->nextAll()
      ->first()
      ->children()
      ->filter($selector);
  }

  /**
   * Checks if a link is a Release link
   * @param \Symfony\Component\DomCrawler\Crawler $tileLink
   *
   * @return bool
   */
  function isReleaseLink(Crawler $tileLink){
    return $this->hasText($this->nextParentsChild($tileLink, 'a[rel="category tag"]'), self::RELEASE_TEXT);
  }

  /**
   * Extract the body
   *
   * @param \Symfony\Component\DomCrawler\Crawler $crawler
   *
   * @return \Symfony\Component\DomCrawler\Crawler
   */
  function body(Crawler $crawler){
    return $crawler->filter("#pagebody .wrapper .col-9");
  }

  /**
   * @param \Symfony\Component\DomCrawler\Crawler $crawler
   *
   * @return Crawler
   */
  public function titleLinks(Crawler $crawler) {
    return $crawler->filter('.fancy a');
  }

  /**
   * Checks if a Dom node has a certain text
   *
   * @param \Symfony\Component\DomCrawler\Crawler $node
   * @param $text
   *
   * @return bool
   */
  public function hasText(Crawler $node, $text){
    return $node->text() == $text;
  }

  /**
   * The a list of release urls
   *
   * @param \Symfony\Component\DomCrawler\Crawler $crawler
   *
   * @return \Illuminate\Support\Collection
   */
  public function releaseLinks(Crawler $crawler) {
    $links = $this->titleLinks($crawler)->reduce(function (Crawler $titleLink) {
      return $this->isReleaseLink($titleLink);
    })->extract(["href", '_text']);

    return collect(array_map(function ($link) {
      return [
        "url" => $link[0],
        'title' => $link[1],
      ];
    }, $links));
  }
}