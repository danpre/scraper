<?php
/**
 * Created by PhpStorm.
 * User: erlik28
 * Date: 5/27/18
 * Time: 11:23 AM
 */

namespace App\Acme\Scraper\Readers;


use App\Acme\Scraper\Contracts\ReadearableContract;
use DonatelloZa\RakePlus\RakePlus;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\DomCrawler\Crawler;

class PostReader implements ReadearableContract {

  /**
   * @param \Symfony\Component\DomCrawler\Crawler $crawler
   * @param \Psr\Http\Message\ResponseInterface|NULL $response
   *
   * @return \Illuminate\Support\Collection
   */
  function read(Crawler $crawler, ResponseInterface $response = null) {
    return collect([
      "description" => $this->metaDescription($crawler),
      "keywords" => $this->metaKeywords($crawler),
      "filesize" => $response ? $response->getBody()->getSize() : null
    ]);
  }

  /**
   * Get the head section
   *
   * @param \Symfony\Component\DomCrawler\Crawler $crawler
   *
   * @return \Symfony\Component\DomCrawler\Crawler
   */
  public function head(Crawler $crawler) {
    return $crawler->filter("head");
  }

  /**
   * Read a meta tag using xpath as Goutte high level dom crawler does not
   * support attribute based - meta tag retrieval
   *
   * @param \Symfony\Component\DomCrawler\Crawler $crawler
   * @param $xpath
   * @param $contentAttribute
   *
   * @return string|null
   */
  public function meta(Crawler $crawler, $xpath, $contentAttribute) {
    $result = $crawler->filterXPath($xpath)
      ->first()
      ->extract($contentAttribute);
    return empty($result) ? NULL : $result[0];
  }

  function metaKeywords(Crawler $crawler) {
    $keywords = $this->meta($crawler, '//meta[@property="og:keywords"]', 'content');
    //if the keywords are not present in the page's head, then infer 5 keywords from
    //the page's body using an implementation of Rake
    if (!$keywords) {
      return $this->inferKeywords($this->body($crawler));
    }
    return array_map("trim", explode(',', $keywords));
  }

  /**
   * Retrieve the meta title
   *
   * @param \Symfony\Component\DomCrawler\Crawler $crawler
   *
   * @return string
   */
  public function metaTitle(Crawler $crawler) {
    return $this->meta($crawler, '//meta[@property="og:title"]', 'content');
  }

  /**
   * Retrieve the meta description
   *
   * @param \Symfony\Component\DomCrawler\Crawler $crawler
   *
   * @return string
   */
  public function metaDescription(Crawler $crawler) {
    return $this->meta($crawler, '//meta[@property="og:description"]', 'content');
  }

  /**
   * Retrieve any meta tag retrieval
   *
   * @param \Symfony\Component\DomCrawler\Crawler $crawler
   * @param $tag
   *
   * @return \Symfony\Component\DomCrawler\Crawler
   */
  function headTag(Crawler $crawler, $tag) {
    return $this->head($crawler)->filter($tag);
  }

  /**
   * Use the RakePlus class to infer keywords from the body of the post
   *
   * @param string $fromText
   *
   * @param int $howMany
   *
   * @return array
   */
  private function inferKeywords($fromText, $howMany = 5) {
    $keywords = RakePlus::create($fromText)
      ->sortByScore(RakePlus::ORDER_DESC)
      ->get();

    return array_slice($keywords, 0, $howMany);
  }

  /**
   * Retrieve the body content of a post assuming the container class is
   * "storycontent"
   *
   * @param \Symfony\Component\DomCrawler\Crawler $crawler
   * @param string $bodySelector
   *
   * @return string
   */
  public function body(Crawler $crawler, $bodySelector = ".storycontent") {
    return trim($crawler->filter($bodySelector)->text());
  }

}