<?php
/**
 * Created by PhpStorm.
 * User: erlik28
 * Date: 5/27/18
 * Time: 1:22 PM
 */

namespace App\Acme\Scraper;


use App\Acme\Scraper\Readers\PageReader;
use App\Acme\Scraper\Readers\PostReader;
use App\Acme\Scraper\Requests\PageRequest;
use App\Acme\Scraper\Requests\PostRequest;

class Scraper {
    public function __construct() {
    }

  /**
   * @param int $page
   *
   * @return \App\Acme\Scraper\ItemsRepository
   * @throws \App\Acme\Scraper\Exceptions\InvalidUrlException
   */
  static function run($page = 1){
    $items = RepositoryFactory::make(new PageRequest(NULL, $page), new PageReader)
      ->map(function ($item) {
        $item += RepositoryFactory::make(new PostRequest($item['url']), new PostReader)
          ->toArray();
        return Item::make($item);
      });
    return new ItemsRepository($items);
    }
}