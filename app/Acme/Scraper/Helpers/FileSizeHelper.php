<?php
/**
 * Created by PhpStorm.
 * User: erlik28
 * Date: 5/28/18
 * Time: 12:30 PM
 */

namespace App\Acme\Scraper\Helpers;


trait FileSizeHelper {

  /**
   * Transform a size from bytes to kilobytes
   *
   * @param $bytesSize
   *
   * @return float|int
   */
  function transformToKb($bytesSize) {
    return floatval($bytesSize) / 1024;
  }

  /**
   * Format the file size
   *
   * @param float $size
   *
   * @param string $measurementUnit
   *
   * @return string
   */
  public function formatSize($size, $measurementUnit = "kB") {
    return number_format($size, 2) . $measurementUnit;
  }}