<?php
/**
 * Created by PhpStorm.
 * User: erlik28
 * Date: 5/27/18
 * Time: 8:46 AM
 */

namespace App\Acme\Scraper\Helpers;


use App\Acme\Scraper\Exceptions\InvalidUrlException;

class UrlBuilder {

  private $url;

  /**
   * UrlBuilder constructor.
   *
   * @param $url
   *
   * @throws \App\Acme\Scraper\Exceptions\InvalidUrlException
   */
  public function __construct($url) {
    $this->url = $this->validateUrl($url);
  }

  /**
   * Small sanity check on the url
   *
   * @param string $url
   *
   * @return string
   * @throws \App\Acme\Scraper\Exceptions\InvalidUrlException
   */
  private function validateUrl($url){
    //validate structure
    if (!filter_var($url, FILTER_VALIDATE_URL)){
      throw new InvalidUrlException();
    }
    //make sure we don't have empty slashes
    $url = rtrim($url, '/');
    return $url;
  }

  /**
   * Append params to the url
   *
   * @param string ...$params
   *
   * @return \App\Acme\Scraper\Helpers\UrlBuilder
   */
  function append(string ...$params){
    $init = explode("/", $this->url);
    foreach ($params as $param){
      $init[] = $param;
    }
    $this->url = implode("/", $init);
    return $this;
  }

  /**
   * Url string value
   *
   * @return string
   */
  function raw(){
    return $this->url;
  }

  /**
   * Url string value
   *
   * @return string
   */
  function __toString(){
    return $this->url;
  }

  /**
   * Url string value
   *
   * @return string
   */
  function url(){
    return $this->url;
  }
}