<?php
/**
 * Created by PhpStorm.
 * User: erlik28
 * Date: 5/28/18
 * Time: 3:23 PM
 */

namespace App\Acme\Scraper\Helpers;


use Illuminate\Http\Request;
use Spatie\ResponseCache\CacheProfiles\CacheAllSuccessfulGetRequests;

class ScraperCacheProfile extends CacheAllSuccessfulGetRequests {

  public function shouldCacheRequest(Request $request): bool {

    if ($this->isRunningInConsole()) {
      return FALSE;
    }

    return $request->isMethod('get');
  }
}