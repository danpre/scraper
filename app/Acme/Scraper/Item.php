<?php
/**
 * Created by PhpStorm.
 * User: erlik28
 * Date: 5/27/18
 * Time: 1:32 PM
 */

namespace App\Acme\Scraper;


use App\Acme\Scraper\Helpers\FileSizeHelper;

class Item {

  use FileSizeHelper;
  /**
   * @var string
   */
  public $url;

  /**
   * @var string
   */
  private $title;

  /**
   * @var string
   */
  private $description;

  /**
   * @var array
   */
  private $keywords;

  /**
   * @var string
   */
  public $filesize;

  /**
   *
   * @param array $params
   *
   * @return static
   */
  static function make(array $params) {
    $item = new static();
    return $item->import($params);
  }

  /**
   * Import data
   * @param array $data
   *
   * @return Item
   */
  function import($data){
    foreach ($data as $key => $value){
      $this->$key = $value;
    }

    return $this;
  }

  function toArray(){
    return [
      'url' => $this->url,
      'link' => $this->title,
      'meta description' => $this->description,
      'keywords' => implode(", ", $this->keywords),
      'filesize' =>  $this->formatSize($this->transformToKb($this->filesize)) ,

    ];
  }

  /**
   * File size getter
   *
   * @param bool $transformedToKb
   *
   * @return float|int|string
   */
  public function fileSize($transformedToKb = false) {
    return $transformedToKb ? $this->transformToKb($this->filesize) : $this->filesize;
  }

}