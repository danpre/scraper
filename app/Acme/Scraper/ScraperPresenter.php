<?php
/**
 * Created by PhpStorm.
 * User: erlik28
 * Date: 5/27/18
 * Time: 7:42 PM
 */

namespace App\Acme\Scraper;


use App\Acme\Scraper\Contracts\PresentableContract;
use Illuminate\Contracts\Support\Responsable;
use function response;

class ScraperPresenter implements Responsable {

  /** @var  \App\Acme\Scraper\Contracts\PresentableContract */
  private $itemCollection;

  /**
   * ScraperPresenter constructor.
   *
   * @param \App\Acme\Scraper\Contracts\PresentableContract $itemCollection
   */
  public function __construct(PresentableContract $itemCollection) {
    $this->itemCollection = $itemCollection;
  }


  /**
   * Create an HTTP response that represents the object.
   *
   * @param  \Illuminate\Http\Request $request
   *
   * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
   */
  public function toResponse($request) {
    return response()->json([
      'results' => $this->itemCollection->toArray(),
      'total' => $this->itemCollection->formattedFileSize()
    ]);
  }
}