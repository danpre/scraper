<?php
/**
 * Created by PhpStorm.
 * User: erlik28
 * Date: 5/27/18
 * Time: 6:55 PM
 */

namespace App\Acme\Scraper;


use App\Acme\Scraper\Contracts\PresentableContract;
use App\Acme\Scraper\Helpers\FileSizeHelper;
use Closure;
use Illuminate\Support\Collection;

class ItemsRepository implements PresentableContract {

  use FileSizeHelper;

  /**
   * @var Collection
   */
  public $items;

  /**
   * ItemsRepository constructor.
   *
   * @param \Illuminate\Support\Collection $items
   */
  public function __construct(Collection $items) {
    $this->items = ($items);
  }


  /**
   * Value proxy
   *
   * @param $variable
   *
   * @return \Illuminate\Support\Collection
   */
  public function values($variable) {
    return $this->items->map(function (Item $item) use ($variable) {
      return $item->$variable;
    });
  }


  /**
   * Map proxy
   *
   * @param \Closure $closure
   *
   * @return \Illuminate\Support\Collection
   */
  public function map(Closure $closure) {
    return $this->items->map($closure);
  }

  /**
   * @return array
   */
  function toArray() {
    $results = [];
    /** @var \App\Acme\Scraper\Item $item */
    foreach ($this->items as $item) {
      $results[] = $item->toArray();
    }
    return $results;
  }

  /**
   * A formatted version of teh size
   *
   * @return string
   */
  function formattedFileSize() {
    return $this->formatSize(
      $this->transformToKb(
        $this->totalFileSizes()
      )
    );
  }

  /**
   * Return the total size of all the items in KB
   *
   * @return int
   */
  function totalFileSizes() {
    return $this->items->sum->filesize;
  }

}